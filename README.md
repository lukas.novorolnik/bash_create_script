# Create bash script
Simple script for creating local project or Gitlab project

## Installation
1. Move all files into directory /Users/username/home/bin
2. Add path to script to .profile
    - export PATH=$PATH:./Users/username/home/bin
3. Make script executable - chmod u+x 'script_name'
4. add alias for 'create' to .bashrc (alias create='source create')
5. In file createScript.py replace  username = 'username' 
   and password = 'password' with your Gitlab username and password
6. If you want to use different editor than VS Code you have to edit "create" file

## Usage
$ create 'name_of_project' - if you want to create local project\
$ create git 'name_of_project' - if you want to create git project

## Author
Lukas Novorolnik


