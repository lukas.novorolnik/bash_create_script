from selenium import webdriver
import selenium
import sys

pName = sys.argv[1]
username = "username"
password = "password"

# Selects and opens Chrome
driver = webdriver.Chrome()
driver.get("https://gitlab.com/users/sign_in")
# Find element with id = user_login
idBoxName = driver.find_element_by_id("user_login")
# Send id info
idBoxName.send_keys(username)
# Setting name and password
idBoxPass = driver.find_element_by_id("user_password")
idBoxPass.send_keys(password)
loginButton = driver.find_element_by_name("commit")
loginButton.click()
# Creating project
driver.get("https://gitlab.com/projects/new")
projectName = driver.find_element_by_id("project_name")
projectName.send_keys(pName)
inicializeWithReadme = driver.find_element_by_id("project_initialize_with_readme")
inicializeWithReadme.click()
createProjectButton = driver.find_element_by_name("commit")
createProjectButton.click()
# Getting ssh clone string
cloneDrop = driver.find_element_by_id("clone-dropdown")
cloneDrop.click()
sshLink = driver.find_element_by_name("ssh_project_clone").get_attribute("value")
print(sshLink)

# Closes browser
driver.quit()


